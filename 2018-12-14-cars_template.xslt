<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <title>Cars</title>
            </head>
            <body>
				<xsl:for-each select="/cars/car">
					<xsl:sort select="@category" order="ascending" />
						<!--<xsl:if test="@category='sport'">--> <!--pokaże tylko sportowe auta-->
							<xsl:if test="position()=last()">	<!--pokaże ostatnie auto only!!!-->
								<div>
									<img>
										<xsl:attribute name="src">
											<xsl:value-of select="photo" />
										</xsl:attribute>
									</img>
									<xsl:value-of select="name" /> (<xsl:value-of select="made_in" />)
								</div>
							</xsl:if>
						<!--</xsl:if>-->
				</xsl:for-each>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>