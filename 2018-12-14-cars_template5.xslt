<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
		<html>
			<head>
				<title> Dokument bez nazwy</title>
				<link rel="Stylesheet" type="text/css" href="style.css"/>
			</head>
			<body>
				
				<xsl:variable name="header">
					<tr>
						<th>Name</th>
						<th>Made in</th>
					</tr>
				</xsl:variable>
				
				<h1>Sport</h1>
				<table>
				<xsl:copy-of select="$header"/>
					<xsl:for-each select="//car[@category='sport']">
						<tr><td><xsl:value-of select="name" /> </td>
						<td>(<xsl:value-of select="made_in" />)</td></tr>
					</xsl:for-each>	
				</table><br/>
				
				<h1>SUV</h1>
				<table>
					<xsl:copy-of select="$header"/>
					<xsl:for-each select="//car[@category='SUV']">
						<tr><td><xsl:value-of select="name" /> </td>
						<td>(<xsl:value-of select="made_in" />)</td></tr>
					</xsl:for-each>
				</table><br/>
				
				<h1>Roadcaster</h1>
				<table>
					<xsl:copy-of select="$header"/>
					<xsl:for-each select="//car[@category='roadster']">
						<tr><td><xsl:value-of select="name" /> </td>
						<td>(<xsl:value-of select="made_in" />)</td></tr>
					</xsl:for-each>
				</table><br/>
			</body>
	    </html>
    </xsl:template>
</xsl:stylesheet>