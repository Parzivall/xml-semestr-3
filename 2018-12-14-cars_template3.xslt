<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
		<html>
			<head>
				<title> Dokument bez nazwy</title>
			</head>
			<body>
				<table border="1">
					<tr><td colspan="3"><b>Sport</b></td></tr>
					<xsl:for-each select="//car[@category='sport']">
						<tr><td><xsl:value-of select="name" /> </td>
						<td>(<xsl:value-of select="made_in" />)</td></tr>
					</xsl:for-each>	
				</table><br/>
				<table border="1">
					<tr><td colspan="3"><b>SUV</b></td></tr>
					<xsl:for-each select="//car[@category='SUV']">
						<tr><td><xsl:value-of select="name" /> </td>
						<td>(<xsl:value-of select="made_in" />)</td></tr>
					</xsl:for-each>
				</table><br/>
				<table border="1">
					<tr><td colspan="3"><b>Roadster</b></td></tr>
					<xsl:for-each select="//car[@category='roadster']">
						<tr><td><xsl:value-of select="name" /> </td>
						<td>(<xsl:value-of select="made_in" />)</td></tr>
					</xsl:for-each>
				</table><br/>
			</body>
	    </html>
    </xsl:template>
</xsl:stylesheet>