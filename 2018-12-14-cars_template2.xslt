<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <title>Cars</title>
            </head>
            <body>
				<xsl:for-each select="/cars/car">
					<xsl:choose>
						<xsl:when test="@category='sport'">
							<div style="color:red">
								<xsl:value-of select="name" /> (<xsl:value-of select="made_in" />)
							</div>
						</xsl:when>
						
						<xsl:when test="@category='SUV'">
							<div style="color:green">
								<xsl:value-of select="name" /> (<xsl:value-of select="made_in" />)
							</div>
						</xsl:when>
						
						<xsl:otherwise>
							<div style="color:orange">
									<xsl:value-of select="name" /> (<xsl:value-of select="made_in" />)
							</div>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</body>
        </html>
    </xsl:template>
</xsl:stylesheet>